#!/usr/bin/make -f
# -*- makefile -*-

VERS := 0.0.3
DATE := 2013-09-02

LAYOUT := ange
ASCIIDOC_HTML := asciidoc --backend=xhtml11 --conf-file=asciidoc/${LAYOUT}.conf

SOURCES_TXT := asciidoc/faq.txt asciidoc/install.txt asciidoc/index.txt
TARGETS_HTML := $(patsubst %.txt, %.html, $(SOURCES_TXT))


all: $(TARGETS_HTML)
	@echo make html

.PHONY: clean
clean:
	@echo make clean
	rm -rf $(TARGETS_HTML)

.PHONY: show_sources
show_sources:
	@echo "make show_sources"
	@echo $(SOURCES_TXT)

.PHONY: help
help:
	@echo make all
	@echo make clean
	@echo make help
	@echo make html
	@echo make show_sources
	@echo make update_www


.PHONY: html
html: $(TARGETS_HTML)

asciidoc/index.html: asciidoc/index.txt
	@echo "$< --> $@"
	$(ASCIIDOC_HTML) -a index-only $<

%.html: %.txt
	@echo "$< --> $@"
	$(ASCIIDOC_HTML) -a toc -a numbered $<


.PHONY: update_www
update_www: html
	@echo "update http://angelstow.net/"
	rsync -rtvuzb --copy-dirlinks --delete --exclude '*~' asciidoc/  elvis.ange.dk:/var/www/angelstow.net/
